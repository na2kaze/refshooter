![image](/uploads/fffe60122733dfeacd136e918fac0005/image.png)

# 概要

- 開発コード: RefShooter
- 開発期間  : 2020/10/24 ~ 2020/11/09
- 開発人数　: 1人


# ゲームルール

制限時間内にできるだけ多く敵を倒しポイントを稼ごう。  
レーザーをたくさん反射させ敵を倒す事によりより多くのポイントを獲得できる。


# 実行ファイル
[こちらからダウンロード](https://gitlab.com/na2kaze/refshooter/-/blob/master/dist/main.exe)


# 作業用メモ
## exe化
```
pyinstaller main.py --noconfirm --onefile --noconsole
```
