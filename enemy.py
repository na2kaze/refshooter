import pygame
from pygame.locals import *
from setting import *
import random
import numpy as np

class Enemy:
    def __init__(self):
        self.left = random.randint(10, WIDTH-20)
        self.top = random.randint(10, HEIGHT-20)
        self.width = 10
        self.height = 10
             
    def draw(self, screen):
        self.Rect = Rect(self.left, self.top, self.width, self.height)
        pygame.draw.rect(screen, RED, self.Rect, 2)

    def move(self):
        self.top -= 1
    
    def deleteCheck(self):
        if self.top <10:
            return True
        else:
            return False

    def intersect(self, p1, p2, p3, p4):
        tc1 = (p1[0] - p2[0]) * (p3[1] - p1[1]) + (p1[1] - p2[1]) * (p1[0] - p3[0])
        tc2 = (p1[0] - p2[0]) * (p4[1] - p1[1]) + (p1[1] - p2[1]) * (p1[0] - p4[0])
        td1 = (p3[0] - p4[0]) * (p1[1] - p3[1]) + (p3[1] - p4[1]) * (p3[0] - p1[0])
        td2 = (p3[0] - p4[0]) * (p2[1] - p3[1]) + (p3[1] - p4[1]) * (p3[0] - p2[0])
        return tc1*tc2<0 and td1*td2<0

    def collisionCheck(self, razerlist):
        result = False
        hitrazer = None
        for razer in razerlist:
            startpoint = razer.pointlist[0].position
            endpoint = razer.pointlist[-1].position
            #レーザの線を定義
            a1 = np.array([startpoint.x, startpoint.y])
            a2 = np.array([endpoint.x, endpoint.y])
            
            # 敵の四隅を定義
            c1 = np.array([self.left, self.top])
            c2 = np.array([self.left + self.width, self.top])
            c3 = np.array([self.left + self.width, self.top + self.height])
            c4 = np.array([self.left, self.height + self.top])

            #それぞれの線ごとに重なりを調べる
            if self.intersect(a1,a2,c1,c2):
                result = True
                hitrazer = razer
            elif self.intersect(a1,a2,c2,c3):
                result = True
                hitrazer = razer
            elif self.intersect(a1,a2,c3,c4):
                result = True
                hitrazer = razer
            elif self.intersect(a1,a2,c4,c1):
                result = True
                hitrazer = razer
            elif result:
                result = True
                hitrazer = razer
            else:
                result = False
            
        return result, hitrazer