import pygame
from pygame.locals import *
import sys
from reflectmath import * 
import os

from enemy import *
from mapflame import *
from player import *
from setting import *
from razer import *
from showAddPoint import *

def ButtonPushCheck(mouse_x, mouse_y ,rect):
    result = False
    if mouse_x >= rect.left and mouse_x <= rect.left + rect.width:
        if mouse_y >= rect.top and mouse_y <= rect.top + rect.height:
            result = True
    return result

def resource_path(relative_path):
    try:
        base_path = sys._MEIPASS
    except Exception:
        base_path = os.path.dirname(__file__)
    return os.path.join(base_path, relative_path)

class Game:
    def __init__(self):
        pygame.init()
        pygame.mixer.init()
        pygame.font.init()
        pygame.display.set_caption("My Game")
        self.screen = pygame.display.set_mode((WIDTH, HEIGHT))
        self.clock = pygame.time.Clock()
        self.running = False
        # point
        self.point = 0

        # exe化されたものか否かによりパス取得方法を変える
        self.fontttf = resource_path('NotoMono-Regular.ttf')

        # objectpool
        self.enemylist = []
        self.bulletlist = []
        self.razerlist = []
        self.addPointUilist = []

        #instance
        self.mf = MapFlame()

        # timer
        self.timer = 30
        self.gametime = GAMETIME
        self.fpscount = FPS

        # gamestate
        self.running = True
        self.gameSence = 0

        # razercooltime
        self.setCoolFPSTime = 0.5 * FPS
        self.currentCoolFPSTime = self.setCoolFPSTime

    def resetCoolFPSTimer(self):
        self.currentCoolFPSTime = self.setCoolFPSTime

    def run(self):
        self.running = True
        while self.running:
            self.clock.tick(FPS)
            self.events()
            self.update()
            self.draw()

    def events(self):
        # event
        
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.running = False
            elif event.type == pygame.MOUSEBUTTONDOWN:
                mouse_x, mouse_y = pygame.mouse.get_pos(())
                center_x, center_y = self.mf.getCenter()
                if self.gameSence == 1:
                    cmx = center_x - mouse_x 
                    cmy = center_y - mouse_y
                    if self.currentCoolFPSTime < 0:
                        self.razerlist.append(Razer(center_x, center_y, -cmx, -cmy))
                        self.resetCoolFPSTimer()


                if ButtonPushCheck(mouse_x,mouse_y,self.buttonRect) and self.gameSence == 0:
                    # titleからingameに 
                    self.gameSence = 1

                if ButtonPushCheck(mouse_x,mouse_y,self.buttonRect) and self.gameSence == 2:
                    # gameoverからtitleに
                    self.gameSence = 0
                    # gameinitialize
                    self.enemylist = []
                    self.bulletlist = []
                    self.razerlist = []
                    self.point = 0
                    self.gametime = GAMETIME

    def update(self):
        mouse_x, mouse_y = pygame.mouse.get_pos(())
        center_x, center_y = self.mf.getCenter()
        corners = self.mf.getCorners()

        self.screen.fill(BLACK)
        self.mf.draw(self.screen)

        if self.gameSence == 0:
            # タイトル画面
            ## タイトルテキスト
            fontH1 = pygame.font.Font(self.fontttf, 60)
            UItitle = fontH1.render(">RefShoot", True, WHITE)
            x, y = fontH1.size(">RefShoot")
            self.screen.blit(UItitle, ((WIDTH/2)-(x/2), (HEIGHT/4)-(y/2)))
            ## Startボタンのテキスト
            font30 = pygame.font.Font(self.fontttf, 30)
            toplay = font30.render("Start", True, WHITE)
            x, y = font30.size("Start")
            self.screen.blit(toplay, ((WIDTH/2)-(x/2), (HEIGHT/2)-(y/2)+30))
            ## Startボタンの枠
            margin = 3
            self.buttonRect = pygame.Rect((WIDTH/2)-(x/2)-(margin*2), (HEIGHT/2)-(y/2)+30, x + margin*4, y + margin-3)
            pygame.draw.rect(self.screen, BLUE, self.buttonRect, 2)

        elif self.gameSence == 1:
            # ゲームを開始
            fontGameUI = pygame.font.Font(self.fontttf, 22)
            text1 = fontGameUI.render("point: " + str(self.point), True, WHITE)
            self.screen.blit(text1, (17,10))
            text2 = fontGameUI.render("time: " + str(self.gametime), True, WHITE)
            self.screen.blit(text2, (17,30))

            # gametimer
            self.fpscount -= 1
            if self.fpscount < 0:
                self.gametime -= 1
                self.fpscount = FPS
            
            self.currentCoolFPSTime -= 1

            # gameover
            if self.gametime < 0:
                self.gameSence = 2

            # Spawner
            self.timer -= 1
            if self.timer <= 0:
                self.timer = 60
                self.enemylist.append(Enemy())
                #bulletlist.append(Bullet())

            # EnemyDraw
            for el in self.enemylist:
                if el.deleteCheck():
                    self.enemylist.remove(el)
                isHit, hitrazer = el.collisionCheck(self.razerlist)
                if isHit:
                    # 加算するポイントを算出
                    addpoint = hitrazer.refrectionTimes + 1
                    # 加算ui表示
                    ui = ShowAddPoint(el.left, el.top, 1 ,addpoint)
                    self.point += addpoint
                    self.addPointUilist.append(ui)
                    self.enemylist.remove(el)

            # 反射ラインの描画
            x_new, y_new = calculate(center_x, center_y, mouse_x, mouse_y, corners)
            line1 = pygame.draw.line(self.screen, GREEN, (center_x, center_y), (x_new, y_new), 2)

            # razerDraw
            for rl in self.razerlist:
                rl.move(WIDTH,HEIGHT)
                if rl.deleteCheck():
                    self.razerlist.remove(rl)

            for ap in self.addPointUilist:
                ap.draw(self.screen)
                if ap.deleteCheck():
                    self.addPointUilist.remove(ap)
            
            for i in self.enemylist:
                i.draw(self.screen)
            for i in self.razerlist:
                i.draw(self.screen)

        elif self.gameSence == 2:
            # GameOverの画面を表示
            ## タイトルテキスト
            fontH1 = pygame.font.Font(self.fontttf, 60)
            UItitle = fontH1.render("gameover", True, WHITE)
            x, y = fontH1.size("gameover")
            self.screen.blit(UItitle, ((WIDTH/2)-(x/2), (HEIGHT/4)-(y/2)))
            ## ボタンのテキスト
            font30 = pygame.font.Font(self.fontttf, 20)
            totitle = font30.render("ToTitle", True, WHITE)
            x, y = font30.size("ToTitle")
            self.screen.blit(totitle, ((WIDTH/2)-(x/2), (HEIGHT/2)-(y/2)+30))
            ## ボタンの枠
            margin = 3
            self.buttonRect = pygame.Rect((WIDTH/2)-(x/2)-(margin*2), (HEIGHT/2)-(y/2)+30, x + margin*3, y + margin)
            pygame.draw.rect(self.screen, BLUE, self.buttonRect, 2)
            ## ポイント表示
            font30 = pygame.font.Font(self.fontttf, 20)
            showpoint = font30.render("point: "+str(self.point), True, WHITE)
            x, y = font30.size("point: "+str(self.point))
            self.screen.blit(showpoint, ((WIDTH/2)-(x/2),120))
        else:
            print("シーンエラーが発生しました。")

        pygame.display.update()

    def draw(self):
        pass

if __name__ == "__main__":
    g = Game()
    g.run()

    pygame.quit()
