import pygame
from pygame.locals import *
from setting import *

class Player: 
    def __init__(self):
        self.CenterWidth = WIDTH/2
        self.CenterHeight = HEIGHT/2

    def draw(self, screen):
        pygame.draw.polygon(screen, GREEN, [[self.CenterWidth-10, self.CenterHeight], 
                                            [self.CenterWidth+10, self.CenterHeight],
                                            [self.CenterWidth,self.CenterHeight+20]])